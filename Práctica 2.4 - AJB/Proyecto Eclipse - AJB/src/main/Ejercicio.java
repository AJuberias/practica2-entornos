package main;
import java.util.Scanner;

public class Ejercicio {
	
	public static void opcion1(){
		System.out.println("http://www.colegiomontessori.com/");
		System.out.println();
	}
	
	public static void opcion2(){
		System.out.println("M�dulo de Entornos de Desarrollo.");
		System.out.println();
	}
	
	
	public static void opcion3(){
		System.out.println("Versi�n 1.02");
		System.out.println();
	}
	
	public static void opcion4(){
		System.out.println("Fin del programa.");
		System.out.println();
	}	

	public static void main(String[] args) {
		
		Scanner lector=new Scanner(System.in);
		int opcion;
		
		do{
		System.out.println("Bienvenido a la pr�ctica 2.4");
		System.out.println("Escoja una opci�n.");
		System.out.println("1. Web");
		System.out.println("2. M�dulo");
		System.out.println("3. Versi�n");
		System.out.println("4. Salir.");
		
		opcion=lector.nextInt();
		
		switch (opcion){
		
		case 1: opcion1();
		break;
		case 2: opcion2();
		break;
		case 3: opcion3();
		break;
		case 4: opcion4();
		break;
		
		default: System.out.println("Opci�n no v�lida.");
		
		}
		} while(opcion!=4);
		
		lector.close();
	}

}
