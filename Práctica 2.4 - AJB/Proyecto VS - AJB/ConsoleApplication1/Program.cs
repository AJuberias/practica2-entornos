﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static void opcion1()
        {
            Console.WriteLine("http://www.colegiomontessori.com/");
            Console.WriteLine();
        }

        static void opcion2()
        {
            Console.WriteLine("Módulo de Entornos de Desarrollo.");
            Console.WriteLine();
        }


        static void opcion3()
        {
            Console.WriteLine("Versión 1.02");
            Console.WriteLine();
        }

        static void opcion4()
        {
            Console.WriteLine("Fin del programa.");
            Console.WriteLine();
        }

        static void Main(String[] args)
        {

       
            int opcion;
            String cad;

            do
            {
                Console.WriteLine("Bienvenido a la práctica 2.4");
                Console.WriteLine("Escoja una opción.");
                Console.WriteLine("1. Web");
                Console.WriteLine("2. Módulo");
                Console.WriteLine("3. Versión");
                Console.WriteLine("4. Salir.");

                cad = Console.ReadLine();
                opcion = int.Parse(cad);

                switch (opcion)
                {

                    case 1:
                        opcion1();
                        break;
                    case 2:
                        opcion2();
                        break;
                    case 3:
                        opcion3();
                        break;
                    case 4:
                        opcion4();
                        break;
                    default:
                        Console.WriteLine("Opción inexistente.");
                        break;

                }
            } while (opcion != 4);
            Console.WriteLine("Pulse una tecla para cerrar...");
            Console.ReadKey();
        }
    }
}
