﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibreriaVS_AndresJuberias;

namespace ComprobarImportacion
{
    class Program
    {
        static void Main(string[] args)
        {
            int x = 84;
            int y = 17;

            EjercicioVS.calculaLaMedia(x, y);
            EjercicioVS.MultiplicaNumeros(x, y);
            EjercicioVS.primo(x);
            EjercicioVS.RestaNumero(x, y);
            EjercicioVS.SumaNumeros(x, y);

            Console.WriteLine("Pulse tecla para continuar...");
            Console.ReadKey();
        }
    }
}
