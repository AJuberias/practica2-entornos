﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibreriaVS_AndresJuberias
{
    public class EjercicioVS
    {

        public static void calculaLaMedia(int num1, int num2)
        {

            int media = (num1 + num2) / 2;
            Console.WriteLine(media);
        }

        public static void SumaNumeros(int num1, int num2)
        {

            int suma = num1 + num2;
            Console.WriteLine(suma);
        }

        public static void RestaNumero(int num1, int num2)
        {

            int dif = num1 - num2;
            Console.WriteLine(dif);
        }

        public static void MultiplicaNumeros(int num1, int num2)
        {

            int producto = num1 * num2;
            Console.WriteLine(producto);
        }

        public static void primo(int num)
        {

            int cont = 0;

            for (int i = 1; i <= num; i++)
            {
                if (num % i == 0)
                {
                    cont++;
                }
            }
            if (cont == 2)
            {
                Console.WriteLine("Es primo");
            }
            else
            {
                Console.WriteLine("No es primo");
            }
        }


        public static void main(String[] args)
        {

        }

    }
}
