package practica23;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JToolBar;
import javax.swing.JSpinner;
import javax.swing.JComboBox;
import javax.swing.JTextPane;
import javax.swing.JButton;
import javax.swing.JSlider;
import javax.swing.JCheckBox;
import java.awt.Toolkit;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JTabbedPane;
import javax.swing.JRadioButton;
import javax.swing.SpinnerNumberModel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
// Pr�ctica 2.3 de Entornos de Desarrollo
/**
 * 
 * @author Andr�s Juber�as Bag��s
 * @since 25/1/2018
 */




public class Eclipse23 extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Eclipse23 frame = new Eclipse23();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Eclipse23() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(Eclipse23.class.getResource("/images/logo_montessori.png")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnArchivo = new JMenu("Archivo");
		menuBar.add(mnArchivo);
		
		JMenuItem mntmGuardar = new JMenuItem("Guardar");
		mnArchivo.add(mntmGuardar);
		
		JMenuItem mntmGuardarCmo = new JMenuItem("Guardar c\u00F3mo...");
		mnArchivo.add(mntmGuardarCmo);
		
		JMenu mnNewMenu = new JMenu("Editar");
		menuBar.add(mnNewMenu);
		
		JMenuItem mntmDeshacer = new JMenuItem("Deshacer");
		mnNewMenu.add(mntmDeshacer);
		
		JMenuItem mntmRehacer = new JMenuItem("Rehacer");
		mnNewMenu.add(mntmRehacer);
		
		JMenu mnNewMenu_1 = new JMenu("Ayuda");
		menuBar.add(mnNewMenu_1);
		
		JMenuItem mntmAyudaGeneral = new JMenuItem("Ayuda general");
		mnNewMenu_1.add(mntmAyudaGeneral);
		
		JMenuItem mntmMsAyuda = new JMenuItem("M\u00E1s ayuda");
		mnNewMenu_1.add(mntmMsAyuda);
		
		JMenu mnNewMenu_2 = new JMenu("Configuraci\u00F3n");
		menuBar.add(mnNewMenu_2);
		
		JMenuItem mntmAjustes = new JMenuItem("Ajustes");
		mnNewMenu_2.add(mntmAjustes);
		
		JMenuItem mntmNewMenuItem = new JMenuItem("Opciones avanzadas");
		mnNewMenu_2.add(mntmNewMenuItem);
		
		JMenuItem mntmVersin = new JMenuItem("Versi\u00F3n");
		mnNewMenu_2.add(mntmVersin);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(10, 45, 271, 185);
		contentPane.add(tabbedPane);
		
		JPanel panel = new JPanel();
		tabbedPane.addTab("Lista", null, panel, null);
		panel.setLayout(null);
		
		JTextPane txtpnErh = new JTextPane();
		txtpnErh.setEditable(false);
		txtpnErh.setBounds(10, 11, 84, 20);
		txtpnErh.setText("Monitor TZ 13");
		panel.add(txtpnErh);
		
		JButton btnActualizar = new JButton("Actualizar");
		btnActualizar.setBounds(77, 134, 107, 23);
		panel.add(btnActualizar);
		
		JTextPane txtpnPantallaTz = new JTextPane();
		txtpnPantallaTz.setEditable(false);
		txtpnPantallaTz.setText("Pantalla TZ 13");
		txtpnPantallaTz.setBounds(10, 42, 84, 20);
		panel.add(txtpnPantallaTz);
		
		JTextPane txtpnTecladoTz = new JTextPane();
		txtpnTecladoTz.setEditable(false);
		txtpnTecladoTz.setText("Teclado TZ 13");
		txtpnTecladoTz.setBounds(10, 73, 84, 20);
		panel.add(txtpnTecladoTz);
		
		JTextPane txtpnRatnTz = new JTextPane();
		txtpnRatnTz.setEditable(false);
		txtpnRatnTz.setText("Rat\u00F3n TZ 13");
		txtpnRatnTz.setBounds(10, 103, 84, 20);
		panel.add(txtpnRatnTz);
		
		JSpinner spinner = new JSpinner();
		spinner.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));
		spinner.setBounds(104, 11, 39, 20);
		panel.add(spinner);
		
		JSpinner spinner_1 = new JSpinner();
		spinner_1.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));
		spinner_1.setBounds(104, 42, 39, 20);
		panel.add(spinner_1);
		
		JSpinner spinner_2 = new JSpinner();
		spinner_2.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));
		spinner_2.setBounds(104, 73, 39, 20);
		panel.add(spinner_2);
		
		JSpinner spinner_3 = new JSpinner();
		spinner_3.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));
		spinner_3.setBounds(104, 103, 39, 20);
		panel.add(spinner_3);
		
		JPanel panel_1 = new JPanel();
		tabbedPane.addTab("Tu carro", null, panel_1, null);
		panel_1.setLayout(null);
		
		JTextPane txtpnTeclado = new JTextPane();
		txtpnTeclado.setText("Teclado");
		txtpnTeclado.setBounds(10, 22, 61, 20);
		panel_1.add(txtpnTeclado);
		
		JTextPane txtpnPantalla = new JTextPane();
		txtpnPantalla.setText("Pantalla");
		txtpnPantalla.setBounds(10, 61, 61, 20);
		panel_1.add(txtpnPantalla);
		
		JTextPane txtpnRatn = new JTextPane();
		txtpnRatn.setText("Rat\u00F3n");
		txtpnRatn.setBounds(10, 102, 61, 20);
		panel_1.add(txtpnRatn);
		
		JButton btnEliminar = new JButton("Eliminar");
		btnEliminar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnEliminar.setBounds(77, 134, 107, 23);
		panel_1.add(btnEliminar);
		
		JCheckBox checkBox = new JCheckBox("");
		checkBox.setBounds(87, 19, 97, 23);
		panel_1.add(checkBox);
		
		JCheckBox checkBox_1 = new JCheckBox("");
		checkBox_1.setBounds(87, 61, 97, 23);
		panel_1.add(checkBox_1);
		
		JCheckBox checkBox_2 = new JCheckBox("");
		checkBox_2.setBounds(87, 99, 97, 23);
		panel_1.add(checkBox_2);
	}
}
