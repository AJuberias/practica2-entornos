package ejercicios;

import java.util.Scanner;

public class Ej4 {

	public static void main(String[] args) {
		/*Establecer un breakpoint en la primera instrucción y avanzar
		 * instrucción a instrucción (step into) analizando el contenido de las variables
		 */
		
		Scanner lector;
		int numeroLeido;
		int resultadoDivision;
		
		lector = new Scanner(System.in);
		System.out.println("Introduce un numero");
		numeroLeido = lector.nextInt();

		for(int i = numeroLeido; i >= 0 ; i--){
			resultadoDivision = numeroLeido / i;
			// ��No se puede dividir por 0!!
			// Eliminando el "0" de "i>=0" solucionar�a el problema.
			System.out.println("el resultado de la division es: " + resultadoDivision);
		}
		
		
		lector.close();
	}

}
